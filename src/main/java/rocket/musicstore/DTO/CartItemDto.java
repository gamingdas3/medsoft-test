package rocket.musicstore.DTO;

import rocket.musicstore.Entities.CartItemEntity;
import rocket.musicstore.Entities.ProductEntity;

public class CartItemDto {
    public CartItemDto(CartItemEntity source){
        ProductEntity sp = source.getProduct();
        id = sp.getId();
        cost = sp.getCost();
        count = source.getCount();
        name = sp.getName();
        path = sp.getPath();
    }
    public int id;
    public int cost;
    public int count;
    public String name;
    public String path;
}
