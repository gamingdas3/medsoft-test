package rocket.musicstore.Entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class CategoryEntity implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String name;

    public CategoryEntity(){}

    public CategoryEntity(String name) {
        this.name = name;
    }

    public CategoryEntity(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }
}
