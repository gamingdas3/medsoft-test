package rocket.musicstore;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import rocket.musicstore.DTO.CartItemDto;
import rocket.musicstore.DTO.OrderDto;
import rocket.musicstore.Entities.*;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
@Path("/")
@Produces({ "application/json" })
public class RestMethods {

    Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    @EJB
    MsmanagerBean manager;


    @GET
    @Path("/addProduct")
    public String AddProduct(
            @QueryParam("name") String name,
            @QueryParam("cost") int cost,
            @QueryParam("category") String categoryName,
            @QueryParam("stock") int stock

    ){
        ProductEntity p = manager.AddProduct(name, cost, categoryName, stock);
        String json = gson.toJson(p);
        return json;
    }

    @GET
    @Path("/addCategory")
    public String AddCategory(
            @QueryParam("name") String name
    ){
        JsonObject json = new JsonObject();

        try {
            CategoryEntity c = manager.AddCategory(name);
            json.addProperty("result", "category added");
        }
        catch (Exception e){
            json.addProperty("error", e.getMessage());
        }

        return gson.toJson(json);

    }


    @GET
    @Path("/signup")
    public String SignUp(@QueryParam("login") String login, @QueryParam("pass") String  pass) {
        try {
            UserEntity user =  manager.CreateUser(login, pass);
            return Token(login, pass);
        }
        catch (Exception e){
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("error", "not unique username");
            return gson.toJson(jsonObject);
        }
    }

    @GET
    @Path("/token")
    public String Token(@QueryParam("login") String login, @QueryParam("pass") String  pass) {
        JsonObject jsonObject = new JsonObject();

        try {
            String userToken =  manager.GetToken(login, pass);
            jsonObject.addProperty("token", userToken);
        }
        catch (Exception e){
            jsonObject.addProperty("error", "auth error: wrong password or username");
        }
        return gson.toJson(jsonObject);
    }



    @GET
    @Path("/product")
    public String GetProduct(@QueryParam("id") int id){
        ProductEntity p = manager.GetProduct(id);
        String json = gson.toJson(p);
        return json;
    }

    @GET
    @Path("/categoriesList")
    public String GetCategories(){
        List<CategoryEntity> ces = manager.GetCategories();
        String json = gson.toJson(ces);
        return json;
    }

    @GET
    @Path("/catalog")
    public String GetProducts(){
        List<ProductEntity> pes = manager.GetAllProducts();
        String json = gson.toJson(pes);
        return json;
    }

    @GET
    @Path("/addtocart")
    public String AddToCart(
            @QueryParam("token") String token,
            @QueryParam("id") int id,
            @QueryParam("count") int count
    ){
        JsonObject jsonObject = new JsonObject();

        try{
            int code = manager.AddToCart(token, id, count);
            if(code == 0){
                jsonObject.addProperty("result", "success");
            }
            else if(code == 1){
                jsonObject.addProperty("result", "denied action");
                jsonObject.addProperty("details", "you ordered this item already");
            }
            else if(code == 2){
                jsonObject.addProperty("result", "denied action");
                jsonObject.addProperty("details", "somebody ordered the last item yet");
            }
            else if(code == 3) {
                jsonObject.addProperty("result", "denied action");
                jsonObject.addProperty("details", "no more than one item of current category ");
            }
        }
        catch (Exception ex){
            jsonObject.addProperty("result", "application error");
            jsonObject.addProperty("details", ex.toString());
        }



        return gson.toJson(jsonObject);
    }

    @GET
    @Path("/cart")
    public String GetCart(@QueryParam("token") String token){
        List<CartItemEntity> products = manager.GetCartItems(token);
        List<CartItemDto> dto = products
                .stream()
                .map( x -> new CartItemDto(x) )
                .collect(Collectors.toList());

        String json = gson.toJson(dto);
        return json;
    }

    @GET
    @Path("/orders")
    public String GetOrders(@QueryParam("token") String token){
        List<OrderEntity> orders = manager.GetOrders(token);
        List<OrderDto> dto = orders
                .stream()
                .map( x -> new OrderDto(x) )
                .collect(Collectors.toList());

        String json = gson.toJson(dto);
        return json;
    }

    @GET
    @Path("/checkout")
    public String Checkout(@QueryParam("token") String token){
        OrderEntity order = manager.CheckoutUser(token);

        String json = gson.toJson(new OrderDto(order));
        return json;
    }
    @GET
    @Path("/retain")
    public String Retain(@QueryParam("token") String token, @QueryParam("id") int id){
        JsonObject jsonObject = new JsonObject();

        try{
            manager.RetainOrder(token, id);
            jsonObject.addProperty("result", "success");
            jsonObject.addProperty("details", "somebody ordered the last item yet");
        }
        catch (Exception ex){
            jsonObject.addProperty("result", "applicationError");
            jsonObject.addProperty("details", ex.toString());
        }
        return gson.toJson(jsonObject);
    }


}