package rocket.musicstore.Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

@Entity
public class OrderEntity implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private double sum;

    private boolean retained;

    @Column(unique = true)
    private Date createdAt;

    @ManyToOne
    UserEntity user;

    @OneToMany
    private Collection<CartItemEntity> cartItems = new LinkedList<CartItemEntity>();

    @OneToOne
    @JoinColumn(name = "payment_id")
    PaymentEntity payment;

    public OrderEntity() {
        this.retained = false;
    }

    public OrderEntity(UserEntity user) {
        this.setRetained(false);
        this.createdAt = new Date();
        this.user = user;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Collection<CartItemEntity> getCartItems() {
        return cartItems;
    }

    public void setCartItems(Collection<CartItemEntity> products) {
        this.cartItems = products;
    }

    public PaymentEntity getPayment() {
        return payment;
    }

    public void setPayment(PaymentEntity payment) {
        this.payment = payment;
    }

    public UserEntity getUser() {
        return user;
    }

    public boolean isRetained() {
        return retained;
    }

    public void setRetained(boolean retained) {
        this.retained = retained;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
