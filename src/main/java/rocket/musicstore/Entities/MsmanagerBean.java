package rocket.musicstore.Entities;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.registry.infomodel.User;
import java.util.*;

@Stateless(name = "msmanagerEJB")
public class MsmanagerBean {
    @PersistenceContext(unitName = "myUnit")
    EntityManager entityManager;


    public MsmanagerBean() {
    }

    public ProductEntity AddProduct(String name, int cost, String categoryName, int stock){
        System.out.println(categoryName);
        CategoryEntity category = entityManager
                .createQuery("from CategoryEntity as c where c.name=?2", CategoryEntity.class)
                .setParameter(2, categoryName)
                .getSingleResult();
        ProductEntity p = new ProductEntity(cost, name,  category, stock, 0);
        entityManager.persist(p);
        return p;
    }

    public ProductEntity GetProduct(int id){
        ProductEntity product = entityManager.find(ProductEntity.class, id);
        entityManager.detach(product);
        return  product;
    }

    public  CategoryEntity AddCategory(String name){
        CategoryEntity c = new CategoryEntity(name);
        entityManager.persist(c);
        return c;
    }

    public List<CategoryEntity> GetCategories(){
        List<CategoryEntity> categoryEntities = entityManager
                .createQuery("from  CategoryEntity", CategoryEntity.class)
                .getResultList();
        //entityManager.detach(categoryEntities);
        return  categoryEntities;
    }

    public UserEntity CreateUser(String login, String pass){
        UserEntity user = new UserEntity();
            user.setLogin(login);
            user.setPass(pass);

        entityManager.persist(user);
        UserEntity userRef = entityManager.getReference(UserEntity.class,  user.getId());

        return userRef;
    }

    public String GetToken(String login, String pass){
        UserEntity found = entityManager
                .createQuery("from UserEntity as u where u.login=?1 and  u.pass=?2", UserEntity.class)
                .setParameter(1, login)
                .setParameter(2, pass)
                .getSingleResult();
        String token = UUID.randomUUID().toString();
        found.setToken(token);
        entityManager.persist(found);
        return token;
    }

    public int GetUserIdByToken(String token){
        int id = entityManager
                .createQuery("from UserEntity as u where u.token = ?1", UserEntity.class)
                .setParameter(1, token)
                .getSingleResult().
                getId();
        return id;
    }

    public UserEntity GetUserByToken(String token){
        UserEntity user = entityManager
                .createQuery("from UserEntity as u where u.token = ?1", UserEntity.class)
                .setParameter(1, token)
                .getSingleResult();
        return user;
    }

    public int AddToCart(String token, int productId, int count){

        UserEntity user = GetUserByToken(token);
        Collection<CartItemEntity> items = GetCartItems(token);
        ProductEntity product = entityManager.find(ProductEntity.class, productId);


        CategoryEntity constrainedCategory = GetCategories().get(0);
        int constrainedId = constrainedCategory.getId();

        boolean  hasConstrainedCategoryProduct = items
                .stream()
                .anyMatch(x -> x.product.getCategory().getId() == constrainedId);

        boolean hadThisProductAlready = items
                .stream()
                .anyMatch(x->x.product.getId() == productId);

        boolean outOfStock = product.getOrdered() >= product.getStock();


        if(hadThisProductAlready){
            return 1;
        }
        else if(outOfStock){
            return 2;
        }
        else if(hasConstrainedCategoryProduct){
            return 3;
        }
        else{
            CartItemEntity item = new CartItemEntity(user, product, count);
            product.setOrdered(product.getOrdered()+1);
            entityManager.persist(item);
            return 0;
        }

    }

    public void RemoveFromCart(String token, int productId, int count){
        UserEntity user = GetUserByToken(token);
        ProductEntity product = entityManager.find(ProductEntity.class, productId);
        user.getCartItems().remove(product);

        entityManager.persist(user);
    }

    public List<CartItemEntity> GetCartItems(String token){
        List<CartItemEntity> result =  entityManager
                .createQuery(
                    "from CartItemEntity  as ci where  ci.user.token =?1 and ci.order = null", CartItemEntity.class)
                .setParameter(1, token)
                .getResultList();
        return result;
    }

    public List<ProductEntity> GetAllProducts(){
        List<ProductEntity> productEntities = entityManager
                .createQuery("from  ProductEntity ", ProductEntity.class)
                .getResultList();
        //entityManager.detach(categoryEntities);
        return  productEntities;
    }

    public OrderEntity CheckoutUser(String token){

        UserEntity user = GetUserByToken(token);


        Collection<CartItemEntity> items = entityManager
                .createQuery("from CartItemEntity as ci where user.id = ?1", CartItemEntity.class)
                .setParameter(1, user.getId())
                .getResultList();


        int sum = 0;
        for (CartItemEntity item: items) {
            sum += item.product.getCost()*item.getCount();
            item.getProduct().setOrdered(item.product.getOrdered()+1);
        }

        int userOrdersCount = entityManager
                .createQuery("from OrderEntity as o where o.user.token = ?1")
                .setParameter(1, token)
                .getResultList()
                .size();

        double multiplier = (userOrdersCount > 1) ? 0.95 : 1.0;

        OrderEntity order = new OrderEntity(user);
        order.setCartItems(items);
        order.setSum(sum * multiplier);


        entityManager.persist(order);
        entityManager.detach(order);

        entityManager
                .createQuery("delete from CartItemEntity as c where c.user.id = ?1")
                .setParameter(1, user.getId())
                .executeUpdate();

        return order;
    }

    public List<OrderEntity> GetOrders(String token){

        List<OrderEntity> orders = entityManager
                .createQuery("from OrderEntity as o where o.user.token = ?1 and o.retained = false", OrderEntity.class)
                .setParameter(1, token)
                .getResultList();

        return orders;
    }

    public void RetainOrder(String token, int orderId){
        UserEntity u = entityManager.createQuery("from UserEntity as u where u.token = ?1", UserEntity.class)
                .setParameter(1, token)
                .getSingleResult();

        OrderEntity o = entityManager
                .createQuery("from OrderEntity as o where o.id = ?1", OrderEntity.class)
                .setParameter(1, orderId)
                .getSingleResult();

        for ( CartItemEntity ci : o.getCartItems()){
            ci.product.setOrdered(ci.product.getOrdered()-1);
        }

        o.setRetained(true);

    }
}
