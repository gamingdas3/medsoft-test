package rocket.musicstore.DTO;

import rocket.musicstore.Entities.CartItemEntity;
import rocket.musicstore.Entities.OrderEntity;

import java.util.ArrayList;
import java.util.List;

public class OrderDto {
    public List<CartItemDto> items;
    public double sum;
    public int id;
    public boolean isPaid;
    public boolean isRetained;

    public OrderDto(OrderEntity source){
        id = source.getId();
        sum = source.getSum();
        isPaid = source.getPayment() != null && source.getPayment().isPaid();
        isRetained = source.isRetained();
        items = new ArrayList<>();

        for (CartItemEntity item: source.getCartItems())
        {
            items.add(new CartItemDto(item));
        }
    }
}
