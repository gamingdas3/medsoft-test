package rocket.musicstore.Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@Entity
public class CartItemEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="product_id")
    ProductEntity product;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="order_id")
    OrderEntity order;

    private int count;

    public UserEntity getUser() {
        return user;
    }

    public CartItemEntity() {
    }

    public CartItemEntity(UserEntity user, ProductEntity product, int count) {
        this.user = user;
        this.count = count;
        this.product = product;
    }

    public CartItemEntity(CartItemEntity source) {
        ProductEntity sp = source.getProduct();
        this.user = source.user;
        this.count = source.count;
        this.product = new ProductEntity(
                sp.getCost(),
                sp.getName() ,
                new CategoryEntity(sp.getCategory().getName(), sp.getCategory().getId()),
                sp.getStock(),
                sp.getOrdered());
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
